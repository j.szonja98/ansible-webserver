# Run playbook:
`ansible-playbook -i env/hosts site.yml`

### Gondolatmenet:
> Kezdetnek elolvastam 1-2 cikket, és megnéztem 1-2 tutorialt arról, hogy hogy szoktak ansible-el automatizálni. Utána, ahogy találtam egy szimpatikus folder structuret, és megértettem hogy mi mit csinál pontosan, már főleg csak doksit meg stackoverflowt használtam. Az volt a terv, hogy egy ec2-ön fogom megfuttatni a playbookot, aztán 1 óra nyűglődés után arra jutottam, hogy gyorsabb lesz megoldani a dolgot localhoston.
Igazából gyorsabb nem volt, viszont cserébe rengeteg olyan errort debuggolhattam ki, amivel még nem találkoztam :D 

## Folder structure:
### - env:
    - inventory.ini // megadja hogy hol fusson a playbook
### - roles:
    - web:
        - nginx:
             - files:
                - index.html // html a static weblaphoz
            - tasks:
                - main.yml // task lista, ami létrehozza és elindítja a webszervert


Nem akartam túlságosan belemenni, nyilván lehetett volna felülcsapni az nginx alapkonfigját egy template fájlal, de a feladat leírásából nem úgy tűnt hogy szükség van rá. 
A playbook egy ubuntun futott, de ha átírjátok a package managert jó eséllyel futni fog máshol is. 





